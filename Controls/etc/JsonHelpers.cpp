#include "JsonHelpers.h"
#include <QJsonDocument>
#include <QDebug>

using namespace SDR;

QJsonObject jsonParser::parse(const char *jsonFormat)
{
  QJsonParseError jErr;
  QJsonDocument jDoc = QJsonDocument::fromJson(QByteArray(jsonFormat), &jErr);
  if (jDoc.isNull())
  {
    qWarning() << "JSON: parse error: " << jErr.errorString() << "\n\"" << jsonFormat << "\"";
    return QJsonObject();
  }
  return jDoc.object();
}

QBoxLayout::Direction jsonParser::direction(QJsonObject &json)
{
  if (!(json.contains("direction") && json["direction"].isString())) return QBoxLayout::TopToBottom;
  QString dirStr = json["direction"].toString();
  QBoxLayout::Direction dir;
  if ((dirStr == "v")||(dirStr == "Vertical")||(dirStr == "TopToBottom"))
    dir = QBoxLayout::TopToBottom;
  else if (dirStr == "BottomToTop")
    dir = QBoxLayout::BottomToTop;

  else if ((dirStr == "h")||(dirStr == "Horisontal")||(dirStr == "LeftToRight"))
    dir = QBoxLayout::LeftToRight;
  else if (dirStr == "RightToLeft")
    dir = QBoxLayout::RightToLeft;
  return dir;
}

int jsonParser::stretch(QJsonObject &json)
{
  if (json.contains("stretch") && json["stretch"].isDouble())
    return json["stretch"].toInt();
  else
    return 0;
}

void jsonGenerator::initContainer(QJsonObject &json, QBoxLayout::Direction dir, QString name, int stretch)
{
  appendDirection(json, dir);
  appendName(json, name);
  appendStretch(json, stretch);
}

void jsonGenerator::initElementParams(QJsonObject & params, int id, QString name)
{
  appendId(params, id);
  appendName(params, name);
}

void jsonGenerator::appendDirection(QJsonObject & json, QBoxLayout::Direction dir)
{
  const char * str;
  switch (dir)
  {
  case QBoxLayout::TopToBottom:
    str = "TopToBottom";
    break;
  case QBoxLayout::BottomToTop:
    str = "BottomToTop";
    break;
  case QBoxLayout::LeftToRight:
    str = "LeftToRight";
    break;
  case QBoxLayout::RightToLeft:
    str = "RightToLeft";
    break;
  }
  json["direction"] = str;
}
void jsonGenerator::appendName(QJsonObject &json, QString name)
{
  if (name.isNull()) return;
  json["name"] = name;
}
void jsonGenerator::appendStretch(QJsonObject &json, int stretch)
{
  json["stretch"] = stretch;
}
void jsonGenerator::appendId(QJsonObject &json, int id)
{
  json["id"] = id;
}
void jsonGenerator::appendElements(QJsonObject &json, QJsonArray &array)
{
  json["elements"] = array;
}

void jsonGenerator::appendContainer(QJsonObject &json, QJsonObject &container)
{
  json["container"] = container;
}
