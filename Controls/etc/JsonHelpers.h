#ifndef JSONHELPERS_H
#define JSONHELPERS_H

#include <QJsonObject>
#include <QJsonArray>

#include <QBoxLayout>

namespace SDR
{

namespace jsonGenerator
{
  void initContainer(QJsonObject &json, QBoxLayout::Direction dir, QString name = QString(), int stretch = 0);
  void initElementParams(QJsonObject &params, int id, QString name);

  void appendId(QJsonObject &json, int id);
  void appendName(QJsonObject &json, QString name);
  void appendDirection(QJsonObject &json, QBoxLayout::Direction dir);
  void appendStretch(QJsonObject &json, int stretch);
  void appendElements(QJsonObject &json, QJsonArray &array);
  void appendContainer(QJsonObject &json, QJsonObject &container);
} // jsonGenerator

namespace jsonParser
{
  QJsonObject parse(const char *jsonFormat);

  QBoxLayout::Direction direction(QJsonObject &json);
  int stretch(QJsonObject &json);
} // jsonParser

} // SDR

#endif // JSONHELPERS_H
