#ifndef TSTATUSWIDGET_H
#define TSTATUSWIDGET_H

#include <QWidget>
#include <QBoxLayout>
#include <QLabel>
#include <ledindicator.h>

#include <QMap>

namespace SDR
{

namespace Status
{
  class TElement : public QWidget
  {
    Q_OBJECT
  public:
    explicit TElement(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      QWidget(parent), Layout(dir,this)
    {
      Layout.setContentsMargins(0,0,0,0);
      setLayout(&Layout);

      this->id = id;
      this->name = name;
    }

    int Id(){return id;}

    void setName(const QString name){this->name = name; refreshName();}

  public slots:
    virtual QString Value() = 0;
    virtual void setValue(const QString &text) = 0;

    virtual void readParams(const QString & paramsStr)
    {
      QStringList params = paramsStr.split(";");
      foreach (const QString &p, params)
      {
        if (p.isEmpty()) continue;
        QStringList x = p.split(":");
        Q_ASSERT(x.count() == 2);
        setParam(x[0],x[1]);
      }
    }
    virtual bool setParam(const QString & name, const QString & value)
    {
      if (name == "name")
      {
        setName(value);
        return true;
      }
      return false;
    }

  protected:
    int id;
    QString name;

    QBoxLayout Layout;

    virtual void refreshName() = 0;
  };

  class TLabledElement : public TElement
  {
    Q_OBJECT
  public:
    explicit TLabledElement(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TElement(id,name,dir,parent)
    {
      Layout.addWidget(&label);
      refreshName();
    }

  public slots:
    void setValue(const QString &text){setName(text);}
    QString Value(){return "";}

  protected:
    QLabel label;

    void refreshName()
    {
      label.setText(name);
      label.setVisible(!name.isEmpty());
    }
  };

  class TLed : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TLed(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent)
    {
      led = new LedIndicator(this);
      led->setLedSize(10);
      led->setOffColor(QColor(Qt::darkGray));
      Layout.addWidget(led);
    }
    LedIndicator * Led(){return led;}
  public slots:
    QString Value(){return led->State() ? "on" : "off" ;}
    void setValue(const QString &text)
    {
      if(text == "on")
        led->setState(true);
      else if (text == "off")
        led->setState(false);
    }
  protected:
    LedIndicator * led;
  };

  class TField : public TLabledElement
  {
    Q_OBJECT
  public:
    explicit TField(const int id, const QString name, QBoxLayout::Direction dir, QWidget * parent = nullptr):
      TLabledElement(id, name, dir, parent)
    {
      valLabel = new QLabel(this);
      valLabel->setFrameShape(QFrame::StyledPanel);
      valLabel->setFrameShadow(QFrame::Sunken);
      valLabel->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred));
      Layout.addWidget(valLabel);
    }
    QLabel * ValueLabel(){return valLabel;}
  public slots:
    QString Value(){return valLabel->text();}
    void setValue(const QString &text)
    {
      valLabel->setText(text);
    }
  protected:
    QLabel * valLabel;
  };
}

class TStatusWidget : public QWidget
{
  Q_OBJECT
public:
  explicit TStatusWidget(QWidget *parent = nullptr);
  explicit TStatusWidget(QBoxLayout::Direction dir = QBoxLayout::TopToBottom, QWidget *parent = nullptr);

  void setDirection(QBoxLayout::Direction dir);

  void NewLine(QBoxLayout::Direction dir = QBoxLayout::LeftToRight);

  void appendToCurrentLine(Status::TElement * element);
  void appendStretchToCurrentLine(int stretch = 0);

  void setIsStarted(bool isRunning);

public slots:
  QString Value(const int id);
  void setValue(const int id, const QString &text);
  void setParams(const int id, const QString &text);

protected:
  QBoxLayout Layout, * curLineLayout;
  QMap<int,Status::TElement*> Elements;

  void createNewLineLayout(QBoxLayout::Direction dir);
};

} // SDR

#endif // TSTATUSWIDGET_H
