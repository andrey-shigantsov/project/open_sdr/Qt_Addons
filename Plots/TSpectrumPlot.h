#ifndef TSPECTRUMPLOT_H
#define TSPECTRUMPLOT_H

#include "TBufferPlot.h"
#include "SDR/BASE/Solvers/samplesCollector.h"

#include "QVector"

namespace SDR
{

class TSpectrumPlot : public TBufferPlot
{
  Q_OBJECT
public:
  explicit TSpectrumPlot(Frequency_t Fs, Size_t Nfft, QWidget *parent = 0);
  ~TSpectrumPlot();

  void clearData();

  void setFs(Frequency_t Fd){setFreqType(Fd);}
  void setNfft(Size_t n);

  void append(Sample_t* samples, Size_t count);
  void append(iqSample_t *samples, Size_t count);
  void append_raw(iqSample_t* samples, Size_t count);
  void append_fft(iqSample_t* samples, Size_t count);

public slots:
  bool setParam(QString & name, QString &value);

protected:
  void setCollectorSize(Size_t n);

  void setBufsSize(Size_t n);

private:
  Size_t Nfft, autoNfft;

  iqSamplesCollector_t Collector;
  QVector<iqSample_t> Buf;
  QVector<Sample_t> magBuf;
};

} // SDR

#endif // TSPECTRUMPLOT_H
